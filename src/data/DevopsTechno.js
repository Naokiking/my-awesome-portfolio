export const DevopsTechno = [
    
    {
        id: 1,
        label: "Ansible",
        css: "ansible",
        img: "./assets/ansible.png",
        description: "Ansible est un outil d'automatisation développé par RedHat. Il permet de mettre en place des 'recettes' nommées 'playbook' qui vont executer les tâches demandées. L'avantage d'Ansible et qu'il nécessite aucun logiciel tiers sur les cibles car il passe par le ssh. Pour le moment j'ai pu expérimenter l'ajout d'utilisateur avec les droits appropriés, l'ajout à un domaine ainsi que l'installation de logiciel de base sur Windows.",
    },
    {
        id: 2,
        label: "Docker",
        css: "docker",
        img: "./assets/docker.png",
        description: "Docker est une technologie qui permet de créer des conteneurs.  Au cours de mon apprentissage, j'ai monté des conteneurs Apache, MySql, Nginx. De plus j'ai mis en place un reverse proxy (conteneurisé) pour relier tous mes conteneurs et facilité l'accès à mes serveurs web. Ce dernier sécurise aussi mes sites en HTTPS grâce à Let's encrypt.  Mon prochain objectif serait de mettre en place des outils de monitoring tels que grafana et/ou prometheus.",
    },
    {
        id: 3,
        label: "Terraform",
        css: "terraform",
        img: "./assets/terraform.png",
        description: "Terraform est un outil de déployement d'infrastructure dite IaC (infrastructure as code). J'ai eu la chance de pouvoir implémenter cet outil dans mon entreprise afin de déployer automatiquement les serveurs nécessaires aux nouveaux clients sur vCenter. Le code est disponible sur mon gitlab",
    }
]