export const projectsData = [
  {
    id: 1,
    title: "EDF",
    date: "Janvier 2020",
    languages: ["PHP", "Mysql", "Materialize", "Github", "Trello"],
    infos:
      "Premier gros projet, permet aux utilisateurs de faire des demandes d'intervention, et au technicien d'accepter ou non les interventions.",
    img: "./assets/img/EDF2.jpg",
    link: "http://projet-edf.tk/",
  },
  {
    id: 2,
    title: "Bloostor GSM",
    date: "Mars 2020",
    languages: ["PHP", "Mysql", "Bulma CSS", "Github", "Trello"],
    infos:
      "Site e-commerce, spécialisé dans les produits informatiques.",
    img: "./assets/img/Bloostor1.jpg",
    link: "https://thochet.fr/Bloostor/",
  },
  {
    id: 3,
    title: "Arrive bientôt",
    date: "Avril 2020",
    languages: ["Projet android", "Java"],
    infos:
      "Projet de l'adaptation bloostor GSM en application mobile",
    img: "./assets/img/travaux.jpg",
    link: "http://www.google.com",
  },

];
