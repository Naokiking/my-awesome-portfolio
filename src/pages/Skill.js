import React from "react";
import SocialNetworks from "../Components/SocialNetworks";
import Navigation from "../Components/Navigation";
import ButtonBottom from "../Components/ButtonBottom";
import { Pdf } from "../Components/Pdf";
import { Mouse } from "../Components/Mouse";

const Skill = () => {
    return(
        <div>
            <Mouse/>
            <main className="project">
                <Navigation />
                <SocialNetworks />
                <Pdf />
                <ButtonBottom right={"/ppe1"} left={"/presentation"}/>
            </main>
        </div>
    );
};

export default Skill;