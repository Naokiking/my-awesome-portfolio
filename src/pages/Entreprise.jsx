import React from "react";
import Navigation from "../Components/Navigation";
import SocialNetworks from "../Components/SocialNetworks";
import ButtonBottom from "../Components/ButtonBottom";
import { Carousal } from "../Components/Carousal";
import { Mouse } from "../Components/Mouse";
import Scroll from "../Components/scroll";

export const Entreprise = () => {
    return(
        <div>
            <Mouse/>
            <div>
            <main className="project">
                <Navigation />
                <SocialNetworks />
                <ButtonBottom right={"/presentation"} left={"/"}/>
                <div className="carousel">
                    <Carousal/>
                </div>
            </main>
            </div>
        </div>
    );

};