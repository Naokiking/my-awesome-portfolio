import React, { useState } from "react";
import { Techno } from "../Components/Techno";
import { DevopsTechno } from "../data/DevopsTechno";
import { TechnoDescription } from "../Components/TechnoDescription";
import Navigation from "../Components/Navigation";
import SocialNetworks from "../Components/SocialNetworks";
import ButtonBottom from "../Components/ButtonBottom";
import { Mouse } from "../Components/Mouse";
import Scroll from "../Components/scroll";

export const DevopsPage = () => {

    const data = DevopsTechno;
    const [currentTechno, setCurrentTechno] = useState(data[0].label);
    const handleCurrentTechno = (label) => setCurrentTechno(label);
    
    return (

        <main className="project">
            <Mouse/>
            <Navigation />
            <SocialNetworks />
            <ButtonBottom left={"/ppe3"} />
            <div className="techno">
                <div className="img-container">
                    <ul>
                    {data.map(techno => 
                    <Techno label={techno.label} css={techno.css} img={techno.img} handleCurrentTechno={handleCurrentTechno}/>
                    )}
                    </ul>
                </div>

                <TechnoDescription description={data.find(elt => elt.label === currentTechno).description} />
            </div>
            <Scroll left={"/ppe3"}/>
        </main>
    );
};



