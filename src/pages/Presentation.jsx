import React from "react";
import ButtonBottom from "../Components/ButtonBottom";
import { Mouse } from "../Components/Mouse";
import Navigation from "../Components/Navigation";
import SocialNetworks from "../Components/SocialNetworks";
import { ImgPres } from "../data/ImgPres";

export const Presentation = () => {

    const data = ImgPres;

    return(
        <div>
            <Mouse/>
            <main className="project">
                <Navigation />
                <SocialNetworks />
                <ButtonBottom left={"/entreprise"} right={"/skill"} />
                <div className="line" />
                <div className="grid-pres">
                    <div className="left-container">
                        <p> Naoki Koyama </p>
                        <div className="avatar">
                            <img src={data[0].img} alt="" />
                        </div>
                        
                    </div>
                    <div className="right-container">
                        <p> Mes projets académiques</p>
                        <div>
                            <ul className='text'>
                                <li> Bac +3 : ANALYSTE EN GENIE INFORMATIQUE ET RESEAUX (Option DevOps)
                                </li>
                                <li>
                                Bac +5 : ARCHITECTE TECHNIQUE EN INFORMATIQUE ET RESEAUX
                                </li>
                                <li>
                                Bac +5 : MASTER SCIENCES, TECHNOLOGIES ET SANTÉ MENTION: INFORMATIQUE PARCOURS RÉSEAUX
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    );
};