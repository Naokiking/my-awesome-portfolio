import React from "react";
import Navigation from "../Components/Navigation";
import Projet from "../Components/Projet";
import SocialNetworks from "../Components/SocialNetworks";
import ButtonBottom from "../Components/ButtonBottom";
import {Mouse} from '../Components/Mouse';

export const PPE1 = () => {
    return(
        <main>
            <Mouse />
            <div className="project">
                <Navigation />
                <SocialNetworks />
                <Projet projectNumber={0} />
                <ButtonBottom left={"/skill"} right={"/ppe2"}/>
            </div>
        </main>
    )
}

export const PPE2 = () => {
    return(
        <main>
            <Mouse />
            <div className="project">
            <Navigation />
            <SocialNetworks />
            <Projet projectNumber={1} />
            <ButtonBottom left={"/ppe1"} right={"/ppe3"}/>
            </div>
        </main>
    )
}

export const PPE3 = () => {
    return(
        <main>
            <Mouse />
            <div className="project">
                <Navigation />
                <SocialNetworks />
                <Projet projectNumber={2} />
                <ButtonBottom left={"/ppe2"} right={"/devops"}/>
            </div>
        </main>
    )
}
