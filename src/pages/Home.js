import React from "react";
import ButtonBottom from "../Components/ButtonBottom";
import { Mouse } from "../Components/Mouse";
import Navigation from "../Components/Navigation";
import SocialNetworks from "../Components/SocialNetworks";


const Home = () => {
    return(
        <div>
            <Mouse/>
            <div className="home">
                <Navigation />
                <SocialNetworks />
                <div className="home-main">
                    <div className="main-content">
                        <div className="container">
                            <div className="box">
                                <div className="title">
                                    <span className="block"></span>
                                    <h1> BIENVENUE <span></span></h1>
                                </div>
                                <div className="role">
                                    <div className="block"></div>
                                    <p> Developpement/DevOps </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ButtonBottom right={"/entreprise"}/>
            </div>
        </div>
    );
};

export default Home;