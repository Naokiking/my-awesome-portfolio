import React from "react";
import { Route, Routes } from 'react-router-dom';
import { DevopsPage } from "./pages/DevopsPage";
import { Entreprise } from "./pages/Entreprise.jsx";
import Home from "./pages/Home";
import { PPE1, PPE2, PPE3 } from "./pages/PPE";
import { Presentation } from "./pages/Presentation";
import Skill from "./pages/Skill";


const App = () => {
  return (
    <Routes>
      <Route exact path="/" element={<Home />} />
      <Route exact path="/entreprise" element={<Entreprise />} />
      <Route exact path="/presentation" element={<Presentation />} />
      <Route exact path="/skill" element={<Skill />} />
      <Route exact path="/ppe1" element={<PPE1 />} /> 
      <Route exact path="/ppe2" element={<PPE2 />} /> 
      <Route exact path="/ppe3" element={<PPE3 />} />
      <Route exact path="/devops" element={<DevopsPage />} />
    </Routes>
  );
};

export default App;
