import React, { useState, useCallback, useEffect } from "react";
import { NavLink } from "react-router-dom";

const  Scroll = (props) => {
    const [y, setY] = useState(window.scrollY);
    const setter = "";
    const handleNavigation = useCallback(
      (e) => {
        const window = e.currentTarget;
        if (y > window.scrollY) {
          setter = "scrollTop";
          console.log("Scroll top");
        } else if (y < window.scrollY) {
            setter = "scrollBottom";
            console.log("Scroll bottom");
        }
        setY(window.scrollY);
      },
      [y]
    );

    useEffect(() => {
        setY(window.scrollY);
        window.addEventListener("scroll", handleNavigation);
    
        return () => {
          window.removeEventListener("scroll", handleNavigation);
        };
      }, [handleNavigation]);
    
    if(setter === "scrollTop"){
        return (
          <NavLink to={props.right}/>  
        );
    } else {
        return (
          <NavLink to={props.left}/>
        );
    }
};

export default Scroll;
    