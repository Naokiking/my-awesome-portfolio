import {Carousel} from '3d-react-carousal';
import { ImgCarousel } from '../data/ImgCarousel';


export const Carousal = () => {

    const data = ImgCarousel;

    let slides = [
        <img  src={data[0].img} alt="1" />,
        <img  src={data[1].img} alt="2" />  ,
        <img  src={data[2].img} alt="3" />  ,
        <img  src={data[3].img} alt="4" />  ,
        <img src={data[4].img} alt="" />
    ];
    return(
        <Carousel slides={slides}/>
    );
}