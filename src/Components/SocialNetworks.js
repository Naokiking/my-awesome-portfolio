import React from "react";

const SocialNetworks = () => {

    const anim = () => {
        let navLinks = document.querySelectorAll('.social-network a');

        navLinks.forEach(link => {
            link.addEventListener('mouseover', (e) =>{
                let attrX = e.offsetX - 20;
                let attrY = e.offsetY + 20;

                link.style.transform = `translate(${attrX}px, ${attrY}px`;
            })

            link.addEventListener('mouseleave', () => {
                link.style.transform = '';
            })
        })
    }
    return(
        <div className="social-network">
            <ul className="content">
                <a href="https://www.facebook.com" className="hover" onMouseOver={anim}>
                    <li> <i className="fab fa-facebook-f" /></li>
                </a>
                <a href="https://www.github.com" className="hover" onMouseOver={anim}>
                    <li> <i className="fab fa-github"/></li>
                </a>
                <a href="https://linkedin.com =" className="hover" onMouseOver={anim}>
                    <li> <i className="fab fa-linkedin-in" /> </li>
                </a>
            </ul>
        </div>
    );
};

export default SocialNetworks;