import {React} from "react";


export const Techno = ({ label, css, img, handleCurrentTechno }) => {
    return (
        <div>
        <li className={css}> <h1> {label} </h1></li>
        <li><img src={img} alt="techno" className="ansible-img" onClick={() => handleCurrentTechno(label)}/></li>
        </div>
    );
};