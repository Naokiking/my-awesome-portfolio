import React from "react";
import { NavLink } from "react-router-dom";

const Navigation = () => {
    return(
        <div className="navigation">
            <ul>
                <NavLink to="/" exact className="hover" activeClassName="nav-active">
                    <li> Accueil </li>
                </NavLink>

                <li className="nav-portfolio">
                    A propos 
                    <ul className="nav-projects">
                        <NavLink to="/entreprise" exact className="hover" activeClassName="nav-active">
                            <li> Mon entreprise </li>
                        </NavLink>

                        <NavLink to="/presentation" exact className="hover" activeClassName="nav-active">
                            <li>  Présentation </li>
                        </NavLink>
                    </ul>
                </li>

                <NavLink to="/skill" exact className="hover" activeClassName="nav-active">
                    <li> Compétences </li>
                </NavLink>

                <li className="nav-portfolio"> 
                    PPE 
                    <ul className="nav-projects">
                        <NavLink to="/ppe1" activeClassName="nav-active">
                            <li>
                                PPE1
                            </li>
                        </NavLink>

                        <NavLink to="/ppe2" activeClassName="nav-active" className="hover">
                            <li>
                                PPE2
                            </li>
                        </NavLink>

                        <NavLink to="/ppe3" activeClassName="nav-active" className="hover">
                            <li>
                                PPE3
                            </li>
                        </NavLink>
                    </ul>
                </li>


                <NavLink to="/devops" exact className="hover" activeClassName="nav-active">
                    <li> Devops </li>
                </NavLink>
            
                
            </ul>
        </div>
    );
};

export default Navigation;