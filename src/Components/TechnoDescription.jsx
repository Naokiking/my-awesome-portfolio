import { React } from 'react';

export const TechnoDescription = ({ description }) => {
    return (

        <div className="text-container">
            <p> {description} </p>
        </div>
    );
}